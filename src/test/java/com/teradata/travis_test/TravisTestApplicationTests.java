package com.teradata.travis_test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TravisTestApplicationTests {

    @Test
    public void contextLoads() {

        Assert.assertEquals(2L,3L-1L);
    }

}
