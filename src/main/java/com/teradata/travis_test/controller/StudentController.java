package com.teradata.travis_test.controller;

import com.teradata.travis_test.model.Student;
import com.teradata.travis_test.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("insert")
    public String insert(Student student) {
        studentService.add(student);
        return "add Success";
    }

    @GetMapping("delete")
    public List<Student> delete(Student student) {
        return studentService.remove(student);
    }


    @GetMapping("update")
    public List<Student> update(Student student) {
        return studentService.update(student);
    }

    @GetMapping("select")
    public List<Student> select(Student student) {
        return studentService.select(student);
    }

    @GetMapping("selectAll")
    public List<Student> selectAll() {
        return studentService.selectAll();
    }
}
