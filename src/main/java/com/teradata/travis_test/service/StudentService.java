package com.teradata.travis_test.service;

import com.teradata.travis_test.model.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class StudentService {

    List<Student> studentList = new ArrayList<>();

    public void add(Student student) {
        studentList.add(student);
    }

    public List<Student> remove(Student student) {
        studentList.remove(student);
        return studentList;
    }

    public List<Student> select(Student student) {
        return studentList.stream().filter(s -> s.getName().contains(student.getName())).collect(Collectors.toList());
    }

    public List<Student> update(Student student) {
        return studentList.stream().map(s -> {
            s.setName(s.getId().equals(student.getId()) ? student.getName() : s.getName());
            return s;
        }).collect(Collectors.toList());
    }

    public List<Student> selectAll() {
        return studentList;
    }
}
